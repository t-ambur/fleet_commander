
// use std::fs;

// extern crate find_folder;

// pub struct BattleConfig {
//     pub num_destroyers_player: u8,
//     pub num_destroyers_ai: u8,
// }
// impl BattleConfig {
//     pub fn new(player_destroyers: u8, ai_destroyers: u8) -> BattleConfig {
//         BattleConfig {
//             num_destroyers_player: player_destroyers,
//             num_destroyers_ai: ai_destroyers,
//         }
//     }
// }

// pub fn _read_in_ship_data() -> BattleConfig {
//     let data_folder = find_folder::Search::ParentsThenKids(3,3).for_folder("data").unwrap();
//     let data = fs::read_to_string(data_folder.join("ship_setup.txt")).expect("Unable to read file");
//     let lines = data.lines();
//     let mut num_d_p: u8 = 1;
//     let mut num_d_ai: u8 = 1;

//     for line in lines {
//         let line_parts: Vec<&str> = line.split('=').collect();
//         if line_parts[0] == "num_destroyers_player" {
//             num_d_p = line_parts[1].parse().unwrap();
//         }
//         if line_parts[0] == "num_destroyers_ai" {
//             num_d_ai = line_parts[1].parse().unwrap();
//         }
//     }
//     BattleConfig {
//         num_destroyers_player: num_d_p,
//         num_destroyers_ai: num_d_ai,
//     }
// }
