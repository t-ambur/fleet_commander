
extern crate find_folder;

use graphics::*;
use opengl_graphics::{GlGraphics, GlyphCache, Texture, TextureSettings};

use crate::resource;
use crate::app::GameState;
use crate::key_handler::KeyID;
use crate::util;

pub static BUTTON_DIM: (u32, u32) = (200, 100);

struct Button {
    unique_id: u8,
    button_text: String,
    pos_top_left: (f64, f64),
    pos_bot_right: (f64, f64),
    highlighted: bool,
    dim: (u32, u32),
}
impl Button {
    pub fn new(starting_pos: (f64, f64), dim: (u32, u32), id: u8, text: &str) -> Button {
        Button {
            pos_top_left: starting_pos,
            pos_bot_right: (starting_pos.0 + dim.0 as f64, starting_pos.1 + dim.1 as f64),
            unique_id: id,
            button_text: String::from(text),
            highlighted: false,
            dim: dim,
        }
    }
    pub fn mouse_inside(&mut self, mouse_pos: (f64, f64)) -> bool {
        self.highlighted = false;
        if mouse_pos.0 > self.pos_top_left.0 && mouse_pos.0 < self.pos_bot_right.0 {
            if mouse_pos.1 > self.pos_top_left.1 && mouse_pos.1 < self.pos_bot_right.1 {
                self.highlighted = true;
            }
        }
        return self.highlighted;
    }
    pub fn get_id(&self) -> u8 {
        return self.unique_id;
    }
    pub fn _is_highlighted(&self) -> bool {
        return self.highlighted;
    }
    pub fn render(&self, button_texture: &Texture, button_h_texture: &Texture, c: &graphics::context::Context, gl: &mut GlGraphics,
                  glyph: &mut GlyphCache) {
        let button_scale_x = self.dim.0 as f64 / BUTTON_DIM.0 as f64;
        let button_scale_y = self.dim.1 as f64 / BUTTON_DIM.1 as f64;
        let mut texture = button_texture;
        if self.highlighted {
            texture = button_h_texture;
        }
        image(texture, 
            c.transform
            .trans(self.pos_top_left.0, self.pos_top_left.1)
            .scale(button_scale_x, button_scale_y),
            gl);
        let font_size = 36;
        text(resource::COLOR_BLACK, font_size, &self.button_text, glyph,
            c.transform.trans(self.pos_top_left.0 + (self.pos_bot_right.0 - self.pos_top_left.0) / 9.0,
            (self.pos_bot_right.1 + self.pos_top_left.1) / 2.0 + (font_size as f64 / 3.0)),
            gl).unwrap();
    }
}

pub struct MainMenu {
    buttons: Vec<Button>,
    button_no_text: Texture,
    button_no_text_h: Texture,
    highlighted_button_id: u8,
    menu_state: u8
}
impl MainMenu {
    pub fn new(starting_pos: (f64, f64), button_spacing: f64) -> MainMenu {
        let assets = find_folder::Search::ParentsThenKids(3, 3).for_folder("assets").unwrap();

        // create and place buttons based on type
        let mut buttons = Vec::new();
        let mut current_y = starting_pos.1;
        buttons.push(Button::new(starting_pos, BUTTON_DIM, 1, "Quick Play"));
        current_y += button_spacing;
        buttons.push(Button::new((starting_pos.0, current_y), BUTTON_DIM, 2, "Campaign"));
        current_y += button_spacing;
        buttons.push(Button::new((starting_pos.0, current_y), BUTTON_DIM, 3, "Options"));
        current_y += button_spacing;
        buttons.push(Button::new((starting_pos.0, current_y), BUTTON_DIM, 4, "Quit"));

        MainMenu {
            buttons: buttons,
            button_no_text: Texture::from_path(assets.join("buttons/basic_button.png"),
                                &TextureSettings::new()).unwrap(),
            button_no_text_h: Texture::from_path(assets.join("buttons/basic_button_h.png"),
                                &TextureSettings::new()).unwrap(),
            menu_state: 0,
            highlighted_button_id: 0
        }
    }

    fn check_mouse_pos(&mut self, mouse_pos: (f64, f64)) {
        let mut found_inside = false;
        for button in &mut self.buttons {
            if button.mouse_inside(mouse_pos) {
                self.highlighted_button_id = button.get_id();
                found_inside = true;
            }
        }
        if !found_inside {
            self.highlighted_button_id = 0;
        }
    }

    pub fn update(&mut self, mouse_pos: (f64, f64)) -> GameState {
        if self.menu_state != 0 {
            let id_to_return = self.convert_id_to_game_state(self.menu_state);
            self.menu_state = 0;
            return id_to_return;
        }
        self.check_mouse_pos(mouse_pos);
        return GameState::MainMenu;
    }

    pub fn mouse_release(&mut self, mouse_button: KeyID) {
        match mouse_button {
            KeyID::MouseLeft => {
                if self.highlighted_button_id != 0 {
                    self.menu_state = self.highlighted_button_id;
                } else {
                    self.menu_state = 0;
                }
            },
            _ => {}
        }
    }

    pub fn render(&self, c: &graphics::context::Context, gl: &mut GlGraphics, glyph: &mut GlyphCache, middle_screen: f64) {
        
        let title_font_size = 64;
        text(resource::COLOR_BLACK, title_font_size, "Fleet Commander", glyph,
        c.transform.trans(middle_screen - 200.0, 125.0), gl)
        .unwrap();

        for button in &self.buttons {
            button.render(&self.button_no_text, &self.button_no_text_h, c, gl, glyph);
        }
    }

    fn convert_id_to_game_state(&self, id: u8) -> GameState {
        match id {
            1 => GameState::QuickShipSetup(0),
            2 => GameState::Campaign,
            3 => GameState::Options,
            4 => GameState::Quit,
            _ => GameState::MainMenu,
        }
    }
}

pub struct ShipMenu {
    buttons: Vec<Button>,
    button_no_text: Texture,
    button_no_text_h: Texture,
    highlighted_button_id: u8,
    menu_state: u8,
    player_text_pos: (f64, f64),
    ai_text_pos: (f64, f64),
    ship_text_starting_y_pos: f64,
    destroyer_counter_x: f64,
    destroyer_counter_ai_x: f64,
    ship_text_buffer: f64,
    pub data: ShipMenuHandler,
}
impl ShipMenu {
    pub fn new(starting_pos: (f64, f64), _button_spacing: f64, max_dim: (u32, u32)) -> ShipMenu {
        let assets = find_folder::Search::ParentsThenKids(3, 3).for_folder("assets").unwrap();

        // create and place buttons based on type
        let mut buttons = Vec::new();
        let current_y = starting_pos.1;
        let text_spacing: f64 = 100.0;
        let tiny_size = 50;
        let tabbed_starting_pos = starting_pos.0 + 180.0;
        let player_minus_x = tabbed_starting_pos + tiny_size as f64 + text_spacing;
        let max_screen_spacing = 10.0;
        let ai_minus_x = max_dim.0 as f64 - tiny_size as f64 - tabbed_starting_pos;
        let ai_plus_x = ai_minus_x - text_spacing - tiny_size as f64;
        let bottom_screen_y = (max_dim.1 - BUTTON_DIM.1) as f64 - max_screen_spacing;
        let bottom_right_button_x = (max_dim.0 - BUTTON_DIM.0) as f64 - max_screen_spacing;
        // player destroyer buttons
        let buffer_value = 100.0;
        let mut buffer = 0.0;
        buttons.push(Button::new((tabbed_starting_pos, starting_pos.1 + buffer), (tiny_size, tiny_size), 2, " -"));
        buttons.push(Button::new((player_minus_x, current_y + buffer), (tiny_size, tiny_size), 1, " +"));
        // ai destroyer buttons
        buttons.push(Button::new((ai_plus_x, current_y + buffer), (tiny_size, tiny_size), 4, " -"));
        buttons.push(Button::new((ai_minus_x, current_y + buffer), (tiny_size, tiny_size), 3, " +"));
        // buttons for cruisers - player
        buffer += buffer_value;
        buttons.push(Button::new((tabbed_starting_pos, starting_pos.1 + buffer), (tiny_size, tiny_size), 8, " -"));
        buttons.push(Button::new((player_minus_x, current_y + buffer), (tiny_size, tiny_size), 7, " +"));
        // ai
        buttons.push(Button::new((ai_plus_x, current_y + buffer), (tiny_size, tiny_size), 10, " -"));
        buttons.push(Button::new((ai_minus_x, current_y + buffer), (tiny_size, tiny_size), 9, " +"));
        // buttons for battleships - player
        buffer += buffer_value;
        buttons.push(Button::new((tabbed_starting_pos, starting_pos.1 + buffer), (tiny_size, tiny_size), 12, " -"));
        buttons.push(Button::new((player_minus_x, current_y + buffer), (tiny_size, tiny_size), 11, " +"));
        // ai
        buttons.push(Button::new((ai_plus_x, current_y + buffer), (tiny_size, tiny_size), 14, " -"));
        buttons.push(Button::new((ai_minus_x, current_y + buffer), (tiny_size, tiny_size), 13, " +"));
        // buttons for carriers - player
        buffer += buffer_value;
        buttons.push(Button::new((tabbed_starting_pos, starting_pos.1 + buffer), (tiny_size, tiny_size), 16, " -"));
        buttons.push(Button::new((player_minus_x, current_y + buffer), (tiny_size, tiny_size), 15, " +"));
        // ai
        buttons.push(Button::new((ai_plus_x, current_y + buffer), (tiny_size, tiny_size), 18, " -"));
        buttons.push(Button::new((ai_minus_x, current_y + buffer), (tiny_size, tiny_size), 17, " +"));
        // buttons for subs - player
        buffer += buffer_value;
        buttons.push(Button::new((tabbed_starting_pos, starting_pos.1 + buffer), (tiny_size, tiny_size), 20, " -"));
        buttons.push(Button::new((player_minus_x, current_y + buffer), (tiny_size, tiny_size), 19, " +"));
        // ai
        buttons.push(Button::new((ai_plus_x, current_y + buffer), (tiny_size, tiny_size), 22, " -"));
        buttons.push(Button::new((ai_minus_x, current_y + buffer), (tiny_size, tiny_size), 21, " +"));
        // control buttons
        buttons.push(Button::new((starting_pos.0, bottom_screen_y), BUTTON_DIM, 5, "Main Menu"));
        buttons.push(Button::new((bottom_right_button_x, bottom_screen_y), BUTTON_DIM, 6, "Start!"));
        
        let player_text_x = tabbed_starting_pos + tiny_size as f64;
        let ai_text_x = max_dim.0 as f64 - player_text_x - text_spacing / 1.5;
        let column_text_y = starting_pos.1 - 20.0;

        ShipMenu {
            buttons: buttons,
            button_no_text: Texture::from_path(assets.join("buttons/basic_button.png"),
                                &TextureSettings::new()).unwrap(),
            button_no_text_h: Texture::from_path(assets.join("buttons/basic_button_h.png"),
                                &TextureSettings::new()).unwrap(),
            menu_state: 0,
            highlighted_button_id: 0,
            player_text_pos: (player_text_x, column_text_y),
            ai_text_pos: (ai_text_x, column_text_y),
            ship_text_starting_y_pos: starting_pos.1 + (BUTTON_DIM.1 as f64 / 3.0),
            destroyer_counter_x: player_minus_x - (text_spacing / 1.8),
            destroyer_counter_ai_x: ai_minus_x - (text_spacing / 1.8),
            ship_text_buffer: buffer_value,
            data: ShipMenuHandler::new(),
        }
    }

    fn check_mouse_pos(&mut self, mouse_pos: (f64, f64)) {
        let mut found_inside = false;
        for button in &mut self.buttons {
            if button.mouse_inside(mouse_pos) {
                self.highlighted_button_id = button.get_id();
                found_inside = true;
            }
        }
        if !found_inside {
            self.highlighted_button_id = 0;
        }
    }

    pub fn update(&mut self, mouse_pos: (f64, f64)) -> GameState {
        if self.menu_state != 0 {
            let id_to_return = self.convert_id_to_game_state(self.menu_state);
            self.menu_state = 0;
            return id_to_return;
        }
        self.check_mouse_pos(mouse_pos);
        return GameState::QuickShipSetup(0);
    }

    pub fn mouse_release(&mut self, mouse_button: KeyID) {
        match mouse_button {
            KeyID::MouseLeft => {
                if self.highlighted_button_id != 0 {
                    self.menu_state = self.highlighted_button_id;
                } else {
                    self.menu_state = 0;
                }
            },
            _ => {}
        }
    }

    pub fn render(&self, c: &graphics::context::Context, gl: &mut GlGraphics, glyph: &mut GlyphCache, middle_screen: f64) {

        // Player and AI columns Text
        let column_font_size = 32;
        let mut buffer = 0.0;
        text(resource::COLOR_BLACK, column_font_size, "Player", glyph,
        c.transform.trans(self.player_text_pos.0, self.player_text_pos.1), gl)
        .unwrap();
        text(resource::COLOR_BLACK, column_font_size, "AI", glyph,
        c.transform.trans(self.ai_text_pos.0, self.ai_text_pos.1), gl)
        .unwrap();
        text(resource::COLOR_BLACK, column_font_size, "Destroyers", glyph,
        c.transform.trans(middle_screen - util::get_spacing_of_string("Destroyers", column_font_size), self.ship_text_starting_y_pos + buffer), gl)
        .unwrap();
        buffer += self.ship_text_buffer;
        text(resource::COLOR_BLACK, column_font_size, "Cruisers", glyph,
        c.transform.trans(middle_screen - util::get_spacing_of_string("Cruisers", column_font_size), self.ship_text_starting_y_pos + buffer), gl)
        .unwrap();
        buffer += self.ship_text_buffer;
        text(resource::COLOR_BLACK, column_font_size, "Battleships", glyph,
        c.transform.trans(middle_screen - util::get_spacing_of_string("Battleships", column_font_size), self.ship_text_starting_y_pos + buffer), gl)
        .unwrap();
        buffer += self.ship_text_buffer;
        text(resource::COLOR_RED, column_font_size, "Carriers", glyph,
        c.transform.trans(middle_screen - util::get_spacing_of_string("Carriers", column_font_size), self.ship_text_starting_y_pos + buffer), gl)
        .unwrap();
        buffer += self.ship_text_buffer;
        text(resource::COLOR_RED, column_font_size, "Submarines", glyph,
        c.transform.trans(middle_screen - util::get_spacing_of_string("Submarines", column_font_size), self.ship_text_starting_y_pos + buffer), gl)
        .unwrap();

        // Counter Text
        // Destroyers
        buffer = 0.0;
        text(resource::COLOR_BLACK, column_font_size, &(self.data.get_destroyer_amount(true).to_string()), glyph,
        c.transform.trans(self.destroyer_counter_x, self.ship_text_starting_y_pos + buffer), gl)
        .unwrap();
        text(resource::COLOR_BLACK, column_font_size, &(self.data.get_destroyer_amount(false).to_string()), glyph,
        c.transform.trans(self.destroyer_counter_ai_x, self.ship_text_starting_y_pos + buffer), gl)
        .unwrap();
        buffer += self.ship_text_buffer;
        // Cruisers
        text(resource::COLOR_BLACK, column_font_size, &(self.data.get_cruiser_amount(true).to_string()), glyph,
        c.transform.trans(self.destroyer_counter_x, self.ship_text_starting_y_pos + buffer), gl)
        .unwrap();
        text(resource::COLOR_BLACK, column_font_size, &(self.data.get_cruiser_amount(false).to_string()), glyph,
        c.transform.trans(self.destroyer_counter_ai_x, self.ship_text_starting_y_pos + buffer), gl)
        .unwrap();
        buffer += self.ship_text_buffer;
        // Battleship
        text(resource::COLOR_BLACK, column_font_size, &(self.data.get_battleship_amount(true).to_string()), glyph,
        c.transform.trans(self.destroyer_counter_x, self.ship_text_starting_y_pos + buffer), gl)
        .unwrap();
        text(resource::COLOR_BLACK, column_font_size, &(self.data.get_battleship_amount(false).to_string()), glyph,
        c.transform.trans(self.destroyer_counter_ai_x, self.ship_text_starting_y_pos + buffer), gl)
        .unwrap();
        buffer += self.ship_text_buffer;
        // Carrier
        text(resource::COLOR_BLACK, column_font_size, &(self.data.get_carrier_amount(true).to_string()), glyph,
        c.transform.trans(self.destroyer_counter_x, self.ship_text_starting_y_pos + buffer), gl)
        .unwrap();
        text(resource::COLOR_BLACK, column_font_size, &(self.data.get_carrier_amount(false).to_string()), glyph,
        c.transform.trans(self.destroyer_counter_ai_x, self.ship_text_starting_y_pos + buffer), gl)
        .unwrap();
        buffer += self.ship_text_buffer;
        // Submarines
        text(resource::COLOR_BLACK, column_font_size, &(self.data.get_sub_amount(true).to_string()), glyph,
        c.transform.trans(self.destroyer_counter_x, self.ship_text_starting_y_pos + buffer), gl)
        .unwrap();
        text(resource::COLOR_BLACK, column_font_size, &(self.data.get_sub_amount(false).to_string()), glyph,
        c.transform.trans(self.destroyer_counter_ai_x, self.ship_text_starting_y_pos + buffer), gl)
        .unwrap();

        // Buttons
        for button in &self.buttons {
            button.render(&self.button_no_text, &self.button_no_text_h, c, gl, glyph);
        }
    }
    pub fn get_handler(&self) -> &ShipMenuHandler {
        return &self.data;
    }

    fn convert_id_to_game_state(&self, id: u8) -> GameState {
        match id {
            5 => GameState::MainMenu,
            6 => GameState::QuickPlay,
            _ => GameState::QuickShipSetup(id),
        }
    }
}

pub struct ShipMenuHandler {
    destroyer_amount: u8,
    ai_destroyer_amount: u8,
    cruiser_amount: u8,
    ai_cruiser_amount: u8,
    battleship_amount: u8,
    ai_battleship_amount: u8,
    carrier_amount: u8,
    ai_carrier_amount: u8,
    sub_amount: u8,
    ai_sub_amount: u8,
    max_ship_type_amount: u8
}
impl ShipMenuHandler {
    pub fn new() -> ShipMenuHandler {
        ShipMenuHandler {
            destroyer_amount: 12,
            ai_destroyer_amount: 12,
            cruiser_amount: 6,
            ai_cruiser_amount: 6,
            battleship_amount: 2,
            ai_battleship_amount: 2,
            carrier_amount: 0,
            ai_carrier_amount: 0,
            sub_amount: 0,
            ai_sub_amount: 0,
            max_ship_type_amount: 15,
        }
    }

    pub fn set_destroyer_count(&mut self, player: bool, increase: bool) {
        let value = if player { self.handle_increment(self.destroyer_amount, increase) }
                    else { self.handle_increment(self.ai_destroyer_amount, increase) };
        if player { self.destroyer_amount = value;
            } else { self.ai_destroyer_amount = value; }
    }
    pub fn set_cruiser_count(&mut self, player: bool, increase: bool) {
        let value = if player { self.handle_increment(self.cruiser_amount, increase) }
                    else { self.handle_increment(self.ai_cruiser_amount, increase) };
        if player { self.cruiser_amount = value;
            } else { self.ai_cruiser_amount = value; }
    }
    pub fn set_battleship_count(&mut self, player: bool, increase: bool) {
        let value = if player { self.handle_increment(self.battleship_amount, increase) }
                    else { self.handle_increment(self.ai_battleship_amount, increase) };
        if player { self.battleship_amount = value;
            } else { self.ai_battleship_amount = value; }
    }
    pub fn set_carrier_count(&mut self, player: bool, increase: bool) {
        let value = if player { self.handle_increment(self.carrier_amount, increase) }
                    else { self.handle_increment(self.ai_carrier_amount, increase) };
        if player { self.carrier_amount = value;
            } else { self.ai_carrier_amount = value; }
    }
    pub fn set_sub_count(&mut self, player: bool, increase: bool) {
        let value = if player { self.handle_increment(self.sub_amount, increase) }
                    else { self.handle_increment(self.ai_sub_amount, increase) };
        if player { self.sub_amount = value;
            } else { self.ai_sub_amount = value; }
    }

    fn handle_increment(&self, value: u8, increase: bool) -> u8 {
        let mut new_value = value;
        if increase {
            if value < self.max_ship_type_amount {
                new_value += 1;
            }
        } else { // decrease
            if value > 0 {
                new_value -= 1;
             }
        }
        return new_value;
    }

    pub fn get_destroyer_amount(&self, player: bool) -> u8 {
        if player {
            return self.destroyer_amount;
        }
        return self.ai_destroyer_amount;
    }
    pub fn get_cruiser_amount(&self, player: bool) -> u8 {
        if player {
            return self.cruiser_amount;
        }
        return self.ai_cruiser_amount;
    }
    pub fn get_battleship_amount(&self, player: bool) -> u8 {
        if player {
            return self.battleship_amount;
        }
        return self.ai_battleship_amount;
    }
    pub fn get_carrier_amount(&self, player: bool) -> u8 {
        if player {
            return self.carrier_amount;
        }
        return self.ai_carrier_amount;
    }
    pub fn get_sub_amount(&self, player: bool) -> u8 {
        if player {
            return self.sub_amount;
        }
        return self.ai_sub_amount;
    }
}