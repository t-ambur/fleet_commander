
use graphics::*;
use opengl_graphics::{GlGraphics, Texture};

use rand::Rng;

use crate::calculation;

pub struct ShipGun {
    time_to_reload: f64,
    reloading_time_remaining: f64,
    ready_to_fire: bool,
    operating: bool,
    damage: u16,
    range: f64,
    arc_height: f64,
    gun_offset: f64,
    damage_vari: f32,
    proj_speed: f32,
    accuracy: f32,
}
impl ShipGun {
    pub fn new_destroyer(gun_offset: f64) -> ShipGun {
        ShipGun {
            time_to_reload: 2.0,
            reloading_time_remaining: 2.0,
            ready_to_fire: false,
            operating: true,
            damage: 15,
            range: 400.0,
            arc_height: -20.0,
            gun_offset: gun_offset,
            damage_vari: 0.20,
            proj_speed: 0.25,
            accuracy: 0.95,
        }
    }
    pub fn new_l_cruiser(gun_offset: f64) -> ShipGun {
        ShipGun {
            time_to_reload: 2.0,
            reloading_time_remaining: 2.0,
            ready_to_fire: false,
            operating: true,
            damage: 20,
            range: 550.0,
            arc_height: -30.0,
            gun_offset: gun_offset,
            damage_vari: 0.20,
            proj_speed: 0.25,
            accuracy: 0.80,
        }
    }
    pub fn new_battleship(gun_offset: f64) -> ShipGun {
        ShipGun {
            time_to_reload: 10.0,
            reloading_time_remaining: 10.0,
            ready_to_fire: false,
            operating: true,
            damage: 40,
            range: 2000.0,
            arc_height: -50.0,
            gun_offset: gun_offset,
            damage_vari: 0.30,
            proj_speed: 0.25,
            accuracy: 0.85,
        }
    }
    pub fn fire(&mut self) -> u16 {
        if self.can_fire() {
            // mark that we are reloading
            self.ready_to_fire = false;
            // start the reload timer
            self.reloading_time_remaining = self.time_to_reload;
            // calc the max possible damage values
            let min_damage: u16 = (self.damage as f32 * (1.0-self.damage_vari)) as u16;
            let max_damage: u16 = (self.damage as f32 * (1.0+self.damage_vari)) as u16;
            // init an rng object
            let mut rng = rand::thread_rng();
            // do a rng roll and return it
            return rng.gen_range(min_damage..max_damage+1);
        }
        0
    }
    pub fn get_arc_height(&self) -> f64 { return self.arc_height; }
    pub fn get_offset(&self) -> f64 { return self.gun_offset; }
    pub fn get_proj_speed(&self) -> f32 { return self.proj_speed; }
    pub fn get_accuracy(&self) -> f32 { return self.accuracy; }
    pub fn can_fire(&self) -> bool {
        return self.ready_to_fire && self.operating
    }
    pub fn reload_guns(&mut self, delta_time: f64) {
        if !self.ready_to_fire {
            self.reloading_time_remaining -= delta_time;
            if self.reloading_time_remaining <= 0.0 {
                self.ready_to_fire = true;
            }
        }
    }
    pub fn can_hit(&self, my_pos: f64, other_pos: f64) -> bool {
        return self.can_fire() && (my_pos+self.gun_offset-other_pos).abs() <= self.range
    }
}

#[derive(Clone, Copy)]
pub struct Tracer {
    current_pos: (f64, f64),
    starting_pos: (f64, f64),
    rot: f64,
    scale: (f64, f64),
    ending_pos: (f64, f64),
    damage: u16,
    movement_speed: f32, 
    active: bool,
    targets_id: u16,
    player_tracer: bool,
    bezier_t: f64,
    bezier_h: f64,
}
impl Tracer {
    pub fn new(starting_pos: (f64, f64), end_pos: (f64, f64), damage: u16, target_id: u16, player_tracer: bool, bezier_height: f64,
                tracer_speed: f32) -> Tracer {
        Tracer {
            current_pos: starting_pos,
            starting_pos: starting_pos,
            rot: 0.0,
            scale: (2.0, 2.0),
            ending_pos: end_pos,
            damage: damage,
            movement_speed: tracer_speed,
            active: true,
            targets_id: target_id,
            player_tracer: player_tracer,
            bezier_t: 0.0,
            bezier_h: bezier_height,
        }
    }

    pub fn render(&self, texture: &Texture, c: &graphics::context::Context, gl: &mut GlGraphics) {
        if self.active {
            image(texture, 
                c.transform
                .trans(self.current_pos.0, self.current_pos.1)
                .rot_deg(self.rot)
                .scale(self.scale.0, self.scale.1),
                gl)
            }
    }
    pub fn move_tracer(&mut self, delta_time: f64) {
        if self.active {
            self.bezier_t += delta_time * self.movement_speed as f64;
            if self.bezier_t > 1.0 {
                self.active = false;
            }
            self.current_pos = calculation::bezier_location(self.starting_pos, self.ending_pos, self.bezier_t, self.bezier_h);
        }
    }
    pub fn is_active(&self) -> bool {
        return self.active;
    }
    pub fn apply_damage(&self) -> (u16, u16, bool) {
        return (self.damage, self.targets_id, self.player_tracer);
    }
}

pub struct AllTracer {
    tracers: Vec<Tracer>,
}
impl AllTracer {
    pub fn new() -> AllTracer {
        let tracers_vec: Vec<Tracer> = Vec::new();
        AllTracer {
            tracers: tracers_vec,
        }
    }
    pub fn add_tracer(&mut self, tracer: Tracer) {
        self.tracers.push(tracer);
    }
    pub fn render(&self, texture: &Texture, c: &graphics::context::Context, gl: &mut GlGraphics) {
        for tracer in &self.tracers {
            tracer.render(texture, c, gl);
        }
    }
    pub fn move_tracers(&mut self, delta: f64) {
        for tracer in &mut self.tracers {
            tracer.move_tracer(delta);
        }
    }
    pub fn update_active(&mut self) -> Vec<Tracer> {
        // copy the current tracers so we can modify the vector
        let current_tracers = self.tracers.clone();
        // the vector to return for damage calculation
        let mut completed_tracers = Vec::new();
        // move the current_tracers that are completed to be returned
        for tracer in current_tracers {
            if !tracer.is_active() {
                completed_tracers.push(tracer);
            }
        }
        // update our tracers, filtering out the completed ones
        self.tracers.retain(|x| x.is_active());
        return completed_tracers;
    }
}
