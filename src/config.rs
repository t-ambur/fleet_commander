/*
    This file acts as the configuration for the rest of the project
*/

use opengl_graphics::{OpenGL};

#[derive(Copy, Clone)]
pub struct Config {
    pub app_name: &'static str,
    pub window_size: [u32; 2],
    pub fullscreen: bool,
    pub opengl: OpenGL,
}
impl Config {
    pub fn new() -> Config {
        Config {
            app_name: "Fleet Commander",
            window_size: [1280, 720],
            fullscreen: false,
            opengl: OpenGL::V3_2, // Change this to OpenGL::V2_1 if not working.
        }
    }
}
