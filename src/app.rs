
use opengl_graphics::{GlyphCache};
use piston::input::{RenderArgs, UpdateArgs};
use crate::{config, resource, ships, weapons, renderer, updater};
use crate::key_handler::KeyID;

pub enum GameState {
    MainMenu,
    QuickShipSetup(u8),
    QuickPlay,
    Campaign,
    Options,
    Quit,
}

pub struct App {
    pub game_config: config::Config,
    pub mouse_pos: (f64, f64),
    pub images: resource::Images,
    pub player_fleet_comp: ships::FleetComposition,
    pub ai_fleet_comp: ships::FleetComposition,
    pub tracers: weapons::AllTracer,
    pub hit_markers: resource::AllMarkers,
    mouse_down: bool,
}
impl App {
    pub fn new(configuration: config::Config, ships: (ships::FleetComposition, ships::FleetComposition)) -> App {
        App {
            game_config: configuration,
            mouse_pos: (0.0, 0.0),
            images: resource::Images::new(),
            player_fleet_comp: ships.0,
            ai_fleet_comp: ships.1,
            tracers: weapons::AllTracer::new(),
            hit_markers: resource::AllMarkers::new(),
            mouse_down: false,
        }
    }

    pub fn render(&mut self, args: &RenderArgs, renderer: &mut renderer::Renderer, glyph: &mut GlyphCache) {
        renderer.clear_screen();
        renderer.draw_battle_scene(args, self, glyph);
    }

    pub fn update(&mut self, args: &UpdateArgs) -> u8 {
        let status: u8 = updater::update(args, self);
        return status;
    }

    pub fn release(&mut self, mouse_button: KeyID) {
        match mouse_button {
            KeyID::MouseLeft => {
                self.mouse_down = true;
            },
            _ => {}
        }
    }

    pub fn get_mouse_status(&mut self) -> bool {
        let mouse_d = self.mouse_down;
        self.mouse_down = false;
        return mouse_d;
    }
}