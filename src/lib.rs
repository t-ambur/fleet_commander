/*
    @author Trevor Amburgey
*/

// TODO refactor u8 ids for buttons with enums

extern crate glutin_window;
extern crate graphics;
extern crate opengl_graphics;
extern crate piston;

use glutin_window::GlutinWindow as Window;
use opengl_graphics::{GlGraphics, GlyphCache, TextureSettings};
use piston::event_loop::{EventSettings, Events};
use piston::input::{RenderEvent, UpdateEvent, ReleaseEvent, MouseCursorEvent};
use piston::window::WindowSettings;


mod app;
mod calculation;
mod config;
mod file_handler;
mod key_handler;
mod menu;
mod renderer;
mod resource;
mod ships;
mod updater;
mod util;
mod weapons;

pub fn program_loop() {
    // create a new configuration struct
    let configuration = config::Config::new();

    // init OpenGL
    let opengl = configuration.opengl;

    // Create an Glutin window, specify our opengl variable as the graphics api
    let mut window: Window = WindowSettings::new(configuration.app_name, configuration.window_size)
        .resizable(false)
        .graphics_api(opengl)
        .exit_on_esc(true)
        .fullscreen(configuration.fullscreen)
        .build()
        .unwrap();


    // Now we can create an OpenGL object, this will be used primarily as our 'drawing' object
    let mut gl: GlGraphics = GlGraphics::new(opengl);

    // struct to handle rendering with OpenGL
    let mut renderer_obj = renderer::Renderer{gl: &mut gl};

    // loop to handle main menu, will dispatch proper screens based on game state
    main_menu(configuration, &mut window, &mut renderer_obj);
}


fn main_menu(configuration: config::Config, window: &mut Window, renderer_obj: &mut renderer::Renderer) {
    let mut default_glyph = GlyphCache::new(resource::get_font_path(), (), TextureSettings::new()).unwrap();

    let first_button_pos_x = (configuration.window_size[0]/2) - (menu::BUTTON_DIM.0/2);
    let first_button_pos_y = (configuration.window_size[1]/2) - (menu::BUTTON_DIM.1) - menu::BUTTON_DIM.1/2;
    let mut main_menu = menu::MainMenu::new((first_button_pos_x as f64, first_button_pos_y as f64), 10.0 + menu::BUTTON_DIM.1 as f64);
    // set up some kind of event handler
    let mut events = Events::new(EventSettings::new());

    // main menu loop
    let mut mouse_pos = (0.0, 0.0);
    while let Some(e) = events.next(window) {
        if let Some(pos) = e.mouse_cursor_args() {
            mouse_pos = (pos[0], pos[1]);
        }

        if let Some(args) = e.render_args() {
            renderer_obj.clear_screen();
            renderer_obj.draw_main_menu(&args, &mut main_menu, &mut default_glyph);
        }

        // if let Some(args) = e.update_args() {
        // }
        let mut game_state = main_menu.update(mouse_pos);
        if !matches!(game_state, app::GameState::MainMenu) {
            game_state = change_state(game_state, configuration, renderer_obj, &mut default_glyph, window);
            if matches!(game_state, app::GameState::Quit) {
                break;
            }
        }

        // if let Some(b) = e.press_args() {
        // }

        if let Some(b) = e.release_args() {
            main_menu.mouse_release(key_handler::return_key(&b));
        }
    }
}

fn change_state(game_state: app::GameState, configuration: config::Config, renderer_obj: &mut renderer::Renderer, default_glyph: &mut GlyphCache,
                window: &mut Window) -> app::GameState {
    match game_state {
        app::GameState::MainMenu => app::GameState::MainMenu,
        app::GameState::Campaign => { println!("Campaign is not yet implemented");
                                      app::GameState::MainMenu },
        app::GameState::Options => { println!("Options is not yet implemented");
                                      app::GameState::MainMenu},
        app::GameState::Quit => app::GameState::Quit, // std::process::exit(0)
        _ => quick_play_menu(configuration, renderer_obj, default_glyph, window),
    }
}

fn quick_play_menu(configuration: config::Config, renderer_obj: &mut renderer::Renderer, default_glyph: &mut GlyphCache, window: &mut Window) -> app::GameState {
    // render ship selection screen
    let mut ship_menu = menu::ShipMenu::new((10.0, 60.0), 10.0 + menu::BUTTON_DIM.1 as f64, (configuration.window_size[0], configuration.window_size[1]));
    let mut events = Events::new(EventSettings::new());
    let mut mouse_pos = (0.0, 0.0);
    while let Some(e) = events.next(window) {
        if let Some(pos) = e.mouse_cursor_args() {
            mouse_pos = (pos[0], pos[1]);
        }

        if let Some(args) = e.render_args() {
            renderer_obj.clear_screen();
            renderer_obj.draw_ship_menu(&args, &mut ship_menu, default_glyph);
        }

        // if let Some(args) = e.update_args() {
        // }
        match ship_menu.update(mouse_pos) {
            app::GameState::MainMenu => return app::GameState::MainMenu,
            app::GameState::QuickShipSetup(button_id) => { match button_id {
                // 1-4 destroyers
                1 => {
                    ship_menu.data.set_destroyer_count(true, true);
                },
                2 => {
                    ship_menu.data.set_destroyer_count(true, false);
                },
                3 => {
                    ship_menu.data.set_destroyer_count(false, true);
                },
                4 => {
                    ship_menu.data.set_destroyer_count(false, false);
                },
                // 5-6 for menu control
                // 7-10 cruisers
                7 => {
                    ship_menu.data.set_cruiser_count(true, true);
                },
                8 => {
                    ship_menu.data.set_cruiser_count(true, false);
                },
                9 => {
                    ship_menu.data.set_cruiser_count(false, true);
                },
                10 => {
                    ship_menu.data.set_cruiser_count(false, false);
                },
                // 11-14 battleships
                11 => {
                    ship_menu.data.set_battleship_count(true, true);
                },
                12 => {
                    ship_menu.data.set_battleship_count(true, false);
                },
                13 => {
                    ship_menu.data.set_battleship_count(false, true);
                },
                14 => {
                    ship_menu.data.set_battleship_count(false, false);
                },
                // 15-18 carriers
                15 => {
                    ship_menu.data.set_carrier_count(true, true);
                },
                16 => {
                    ship_menu.data.set_carrier_count(true, false);
                },
                17 => {
                    ship_menu.data.set_carrier_count(false, true);
                },
                18 => {
                    ship_menu.data.set_carrier_count(false, false);
                },
                // 19-22 subs
                19 => {
                    ship_menu.data.set_sub_count(true, true);
                },
                20 => {
                    ship_menu.data.set_sub_count(true, false);
                },
                21 => {
                    ship_menu.data.set_sub_count(false, true);
                },
                22 => {
                    ship_menu.data.set_sub_count(false, false);
                },
                _ => {},
            }},
            app::GameState::QuickPlay => break,
            _ => {}
        }

        // if let Some(b) = e.press_args() {
        // }

        if let Some(b) = e.release_args() {
            ship_menu.mouse_release(key_handler::return_key(&b));
        }
    }

    let ships = ships::init_ships(ship_menu.get_handler(), configuration.window_size[0], configuration.window_size[1]);
    return quick_play(configuration, renderer_obj, default_glyph, window, ships);
    
}

fn quick_play(configuration: config::Config, renderer_obj: &mut renderer::Renderer, default_glyph: &mut GlyphCache, window: &mut Window,
            ships: (ships::FleetComposition, ships::FleetComposition)) -> app::GameState {
    // Create a new game struct
    let mut app = app::App::new(configuration, ships);
    // This is the main battle loop
    let mut victory_status = 0;
    let mut events = Events::new(EventSettings::new());
    while let Some(e) = events.next(window) {

        if let Some(pos) = e.mouse_cursor_args() {
            app.mouse_pos = (pos[0], pos[1]);
        }

        if let Some(args) = e.render_args() {
            app.render(&args, renderer_obj, default_glyph);
            if victory_status != 0 {
                renderer_obj.draw_end_of_game(&args, default_glyph, victory_status);
            }
        }

        if let Some(args) = e.update_args() {
            let status = app.update(&args);
            if status != 0 && victory_status == 0 {
                victory_status = status;
            }
        }

        // if let Some(b) = e.press_args() {
        //     app.press(&b);
        // }

        if let Some(b) = e.release_args() {
            app.release(key_handler::return_key(&b));
        }

        let mouse_down = app.get_mouse_status();
        if victory_status != 0 && mouse_down {
            break;
        }
    }
    return app::GameState::MainMenu;
}