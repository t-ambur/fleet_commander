
use opengl_graphics::{GlGraphics, GlyphCache};
use piston::input::{RenderArgs};
use graphics::*;

use crate::resource;
use crate::menu;
use crate::app::App;

// render using a closure:
// self.gl.draw(args.viewport(), |c, gl| {
pub struct Renderer<'a> {
    pub gl: &'a mut GlGraphics
}

impl Renderer<'_> {
    pub fn clear_screen(&mut self,) {
        clear(resource::COLOR_BLUE, self.gl);
    }

    pub fn draw_battle_scene(&mut self, args: &RenderArgs, app: &mut App, glyph: &mut GlyphCache) {
        let line_spacing: f64 = args.window_size[0] / 6.0;
        
        let c = self.gl.draw_begin(args.viewport());

        for i in 0..5 {
            if i == 5/2 {
                continue;
            }
            line(resource::COLOR_BLACK, 1.0,
                [(i+1) as f64 * line_spacing, 0.0,
                (i+1) as f64 * line_spacing, args.window_size[1]],
                c.transform, self.gl);
        }
        app.player_fleet_comp.render(&c, self.gl);
        app.ai_fleet_comp.render(&c, self.gl);
        app.tracers.render(&app.images.basic_tracer, &c, self.gl);
        app.hit_markers.render(&c, self.gl, glyph);
        
        self.gl.draw_end();
    }

    pub fn draw_main_menu(&mut self, args: &RenderArgs, menu: &mut menu::MainMenu, glyph: &mut GlyphCache) {
        let middle_screen = args.window_size[0] / 2.0;
        let c = self.gl.draw_begin(args.viewport());
        menu.render(&c, &mut self.gl, glyph, middle_screen);
        self.gl.draw_end();
    }

    pub fn draw_ship_menu(&mut self, args: &RenderArgs, menu: &mut menu::ShipMenu, glyph: &mut GlyphCache) {
        let middle_screen = args.window_size[0] / 2.0;
        let c = self.gl.draw_begin(args.viewport());
        menu.render(&c, &mut self.gl, glyph, middle_screen);
        self.gl.draw_end();
    }

    pub fn draw_end_of_game(&mut self, args: &RenderArgs, glyph: &mut GlyphCache, status: u8) {
        let text_size = 128;
        let victory_text = if status == 1 {
            "Victory!"
        } else if status == 2 {
            "Defeat!"
        } else {
            "Draw!"
        };
        let color = if status == 1 {
            resource::COLOR_GREEN
        } else if status == 2 {
            resource::COLOR_RED
        } else {
            resource::COLOR_BLACK
        };

        let c = self.gl.draw_begin(args.viewport());
        text(color, text_size, &victory_text, glyph, c.transform
            .trans(args.window_size[0] / 2.0 - text_size as f64, args.window_size[1] / 2.0), self.gl)
            .unwrap();
        self.gl.draw_end();
    }
}
