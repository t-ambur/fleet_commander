/*
    This file acts as a manager for creating resource structs.
    Would be nice to implement some kind of polymorphism here.
*/

extern crate find_folder;

use opengl_graphics::{TextureSettings, Texture, GlyphCache, GlGraphics};
use graphics::*;

use rand::Rng;

// GLOBAL CONSTANT COLORS
pub static COLOR_BLACK: [f32; 4] = [0.0, 0.0, 0.0, 1.0];
pub static COLOR_RED: [f32; 4] = [1.0, 0.0, 0.0, 1.0];
pub static COLOR_GREEN: [f32; 4] = [0.0, 1.0, 0.0, 1.0];
pub static COLOR_BLUE: [f32; 4] = [0.0, 0.4, 1.0, 1.0];

// STRUCTS FOR SPECIFIC, COMPLEX RESOURCES
pub struct Images {
    pub rust_logo: Texture,
    pub h_cruiser: Texture,
    pub carrier: Texture,
    pub aircraft: Texture,
    pub submarine: Texture,
    pub basic_tracer: Texture,
}

// TODO check unwraps for panics
impl Images {
    pub fn new() -> Images {
        let assets = find_folder::Search::ParentsThenKids(3, 3).for_folder("assets").unwrap();

        Images {
            rust_logo: Texture::from_path(assets.join("rust.png"), 
                                &TextureSettings::new()).unwrap(),
            h_cruiser: Texture::from_path(assets.join("ships/Hcruiser.png"),
                                &TextureSettings::new()).unwrap(),
            carrier: Texture::from_path(assets.join("ships/carrier.png"),
                                &TextureSettings::new()).unwrap(),
            aircraft: Texture::from_path(assets.join("f35.png"),
                                &TextureSettings::new()).unwrap(),
            submarine: Texture::from_path(assets.join("ships/submarine.png"),
                                &TextureSettings::new()).unwrap(),
            basic_tracer: Texture::from_path(assets.join("1px_tracer.png"),
                                &TextureSettings::new()).unwrap(),
        }
    }
}

pub fn get_font_path() -> std::path::PathBuf {
    let assets = find_folder::Search::ParentsThenKids(3, 3).for_folder("assets").unwrap();
    return assets.join("FiraSans-Regular.ttf");
}

pub struct HitMarker {
    hit: bool,
    texture: opengl_graphics::Texture,
    damage: u16,
    pos_hit: (f64, f64),
    pos_text: (f64, f64),
    font_size: u32,
    lifetime_remaining_hit: f64,
    lifetime_remaining_text: f64,
    rot: f64,
    scale: (f64, f64),
    movement_speed: f64,
}
impl HitMarker {
    pub fn new(hit: bool, damage: u16, starting_pos: (f64, f64)) -> HitMarker {
        let assets = find_folder::Search::ParentsThenKids(3, 3).for_folder("assets").unwrap();

        let hit_marker = if hit {
            Texture::from_path(assets.join("hit_marker_basic.png"),
                               &TextureSettings::new()).unwrap()
        } else {
            Texture::from_path(assets.join("miss_marker_basic.png"),
                               &TextureSettings::new()).unwrap()
        };

        let mut rng = rand::thread_rng();
        let text_pos_range = 10.0;
        let text_pos_x = rng.gen_range(starting_pos.0 - text_pos_range..starting_pos.0 + text_pos_range+1.0);
        let text_pos_y = rng.gen_range(starting_pos.1 - text_pos_range..starting_pos.1 + text_pos_range + 1.0);

        HitMarker {
            hit: hit,
            texture: hit_marker,
            damage: damage,
            pos_hit: starting_pos,
            pos_text: (text_pos_x, text_pos_y),
            font_size: 16,
            lifetime_remaining_hit: 2.0,
            lifetime_remaining_text: 3.0,
            rot: 0.0,
            scale: (0.5, 0.5),
            movement_speed: 30.0,
        }
    }

    pub fn render(&self, c: &graphics::context::Context, gl: &mut GlGraphics, glyph: &mut GlyphCache) {
        if self.lifetime_remaining_hit > 0.0 {
            image(&self.texture, 
                c.transform
                .trans(self.pos_hit.0, self.pos_hit.1)
                .rot_deg(self.rot)
                .scale(self.scale.0, self.scale.1),
                gl);
            }
        if self.lifetime_remaining_text > 0.0 {
            let display_text = if self.hit {
                self.damage.to_string()
            } else {
                String::from("Miss")
            }.to_string();
            text(COLOR_BLACK, self.font_size, &display_text, glyph, c.transform
                .trans(self.pos_text.0, self.pos_text.1), gl).unwrap();
        }
    }
    pub fn move_markers(&mut self, delta_time: f64) {
        let speed = delta_time * self.movement_speed;
        if self.lifetime_remaining_text > 0.0 {
            self.pos_text.1 -= speed;
        }
        self.lifetime_remaining_text -= delta_time;
        self.lifetime_remaining_hit -= delta_time;
    }
    pub fn is_active(&self) -> bool {
        return self.lifetime_remaining_hit > 0.0 || self.lifetime_remaining_text > 0.0;
    }
}

pub struct AllMarkers {
    markers: Vec<HitMarker>,
}
impl AllMarkers {
    pub fn new() -> AllMarkers {
        let all_markers: Vec<HitMarker> = Vec::new();
        AllMarkers {
            markers: all_markers,
        }
    }

    pub fn render(&self, c: &graphics::context::Context, gl: &mut GlGraphics, glyph: &mut GlyphCache) {
        for marker in &self.markers {
            marker.render(c, gl, glyph);
        }
    }
    pub fn move_markers(&mut self, delta_time: f64) {
        for marker in &mut self.markers {
            marker.move_markers(delta_time);
        }
        self.markers.retain(|x| x.is_active());
    }
    pub fn add_marker(&mut self, marker: HitMarker) {
        self.markers.push(marker);
    }
}