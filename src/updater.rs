use piston::input::{UpdateArgs};

use crate::app::App;
use crate::calculation;

pub fn update(args: &UpdateArgs, app: &mut App) -> u8 {
    // app.example_data.rotation += app.example_data.rotation_speed * args.dt;
    
    // find the middle of the window
    let middle_screen = app.game_config.window_size[0] as f64 / 2.0;
    // move the ships forward until they reach the middle
    app.player_fleet_comp.move_forward(middle_screen, args.dt);
    app.ai_fleet_comp.move_forward(middle_screen, args.dt);
    // update the fleets for internal updates such as reloading shells
    app.player_fleet_comp.update(args.dt);
    app.ai_fleet_comp.update(args.dt);
    // have the ships shoot at each other if they are able to
    calculation::firing_phase(&mut app.player_fleet_comp, &mut app.ai_fleet_comp, &mut app.tracers);
    // update the tracer position
    app.tracers.move_tracers(args.dt);
    // damage the targeted ships if the tracers have reached their destination
    calculation::apply_damage(app);
    // show hit markers and floating text
    app.hit_markers.move_markers(args.dt);
    // check if either fleet was defeated
    let player_alive = app.player_fleet_comp.update_alive_ships();
    let ai_alive = app.ai_fleet_comp.update_alive_ships();
    return check_battle_status(player_alive, ai_alive)
}

fn check_battle_status(player_alive: bool, ai_alive: bool) -> u8 {
    if !player_alive {
        if !ai_alive {
            // print!("\nDraw!\n");
            return 3;
        }
        else {
            // print!("\nDefeat!\n");
            return 2;
        }
    }
    else if !ai_alive {
        if !player_alive {
            // print!("\nDraw\n");
            return 3;
        }
        else {
            // print!("\nVictory!\n");
            return 1;
        }
    }
    return 0;
}
