
use piston::input::{MouseButton, Button, Key};

pub enum KeyID {
    MouseLeft,
    MouseRight,
    ArrowUp,
    ArrowDown,
    None,
}

pub fn return_key(args: &Button) -> KeyID {
    if let &Button::Mouse(mouse) = args {
        match mouse {
            MouseButton::Left => return KeyID::MouseLeft,
            MouseButton::Right => return KeyID::MouseRight,
            _ => {},
        }
    }
    else if let &Button::Keyboard(key) = args {
        match key {
            Key::Up => return KeyID::ArrowUp,
            Key::Down => return KeyID::ArrowDown,
            _ => {},
        }
    } // else return none
    KeyID::None
}