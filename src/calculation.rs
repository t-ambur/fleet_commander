
use std::collections::HashMap;
use rand::Rng;

use crate::ships;
use crate::weapons;
use crate::app::App;

pub fn firing_phase(player_fleet: &mut ships::FleetComposition, ai_fleet: &mut ships::FleetComposition, tracers: &mut weapons::AllTracer) {
    // both teams will take action at the 'same time'
    // first they will see what ships they can hit
    let mut rng = rand::thread_rng();
    let firing_vec_player = get_firable(player_fleet, ai_fleet);
    for firing_ship_id in firing_vec_player.keys() {
        // look up the targets for the firing ship, grab the first one, and shoot
        let vector_to_shoot_at = &firing_vec_player[firing_ship_id];
        if vector_to_shoot_at.len() <= 0 {
            continue;
        }
        let target_index = rng.gen_range(0..vector_to_shoot_at.len());
        let ship_id_to_shoot_at = &vector_to_shoot_at[target_index];
        fire_at_ship(player_fleet, *firing_ship_id, ai_fleet, *ship_id_to_shoot_at, tracers);
    }

    let firing_vec_ai = get_firable(ai_fleet, player_fleet);
    for firing_ship_id in firing_vec_ai.keys() {
        // look up the targets for the firing ship, grab the first one, and shoot
        let vector_to_shoot_at = &firing_vec_ai[firing_ship_id];
        if vector_to_shoot_at.len() <= 0 {
            continue;
        }
        let target_index = rng.gen_range(0..vector_to_shoot_at.len());
        let ship_id_to_shoot_at = &vector_to_shoot_at[target_index];
        fire_at_ship(ai_fleet, *firing_ship_id, player_fleet, *ship_id_to_shoot_at, tracers);
    }
}

fn fire_at_ship(firing_fleet: &mut ships::FleetComposition, firing_ship_id: u16, target_fleet: &mut ships::FleetComposition,
                damaged_ship_id: u16, tracers: &mut weapons::AllTracer) -> () {
    for firing_ship in &mut firing_fleet.afloat_ships {
        if firing_ship_id == firing_ship.get_id() {
            for potential_target in &mut target_fleet.afloat_ships {
                if damaged_ship_id == potential_target.get_id() {
                    let shot_tracers = firing_ship.attack(potential_target.get_pos(), potential_target.get_id());
                    for tracer in shot_tracers {
                        tracers.add_tracer(tracer);
                    }
                }
            }
        }
    }
}

fn get_firable(firing_fleet: &mut ships::FleetComposition, targeted_fleet: &mut ships::FleetComposition) 
                      -> HashMap<u16, Vec<u16>> {
    let mut firing_map = HashMap::new();
    for firing_ship in &mut firing_fleet.afloat_ships {
        let mut firing_vec = Vec::new();
        for target_ship in &targeted_fleet.afloat_ships {
            if firing_ship.can_hit(target_ship.get_pos().0) {
                firing_vec.push(target_ship.get_id());
            }
        }
        if firing_vec.len() > 0 {
            firing_map.insert(firing_ship.get_id(), firing_vec);
        }
    }
    return firing_map;
}

pub fn apply_damage(app: &mut App) {
    let completed_tracers = app.tracers.update_active();
    if completed_tracers.len() > 0 {
        for tracer in completed_tracers {
            let tracer_tuple = tracer.apply_damage();
            let damage = tracer_tuple.0;
            let damaged_ship_id = tracer_tuple.1;
            let player_controlled = tracer_tuple.2;
            if player_controlled {
                for ship in &mut app.ai_fleet_comp.afloat_ships {
                    if ship.get_id() == damaged_ship_id {
                        let marker = ship.take_damage(damage);
                        app.hit_markers.add_marker(marker);
                    }
                }
            } else {
                for ship in &mut app.player_fleet_comp.afloat_ships {
                    if ship.get_id() == damaged_ship_id {
                        let marker = ship.take_damage(damage);
                        app.hit_markers.add_marker(marker);
                    }
                }
            }
        }
    }
}

fn bezier_curve(z1: f64, z2: f64, z3: f64, t: f64) -> f64 {
    return (1.0-t)*(1.0-t)*z1 + 2.0*(1.0-t)*t*z2 + t*t*z3;
}

pub fn bezier_location(p1: (f64, f64), p3: (f64, f64), t: f64, y_height: f64) -> (f64, f64) {
    // the smaller y value will be used for the p2 y
    let mut p2y:f64 = if p1.1 < p3.1 { p1.1 } else { p3.1 };
    p2y = p2y + y_height;
    // get the bezier points for each x and y
    let x:f64 = bezier_curve(p1.0, (p3.0+p1.0)/2.0, p3.0, t);
    let y:f64 = bezier_curve(p1.1, p2y, p3.1, t);
    return (x,y);    
}
