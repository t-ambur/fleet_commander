
// use graphics::context::{Context};
use opengl_graphics::{GlGraphics, Texture, TextureSettings};
use graphics::*;
use rand::Rng;
use crate::menu;
use crate::weapons;
use crate::resource;

pub trait Ship {
    // abstract
    fn get_pos(&self) -> (f64, f64);
    fn get_scale(&self) -> (f64, f64);
    fn get_rot(&self) -> f64;
    fn get_movement_speed(&self) -> f64;
    fn get_middle_buffer(&self) -> f64;
    fn get_evasion(&self) -> f32;
    fn get_id(&self) -> u16;
    fn get_hp(&self) -> u16;
    fn get_guns(&mut self) -> &mut Vec<weapons::ShipGun>;
    fn get_texture(&self) -> &Texture;
    fn is_alive(&self) -> bool;
    fn is_player(&self) -> bool;
    fn is_reversing(&self) -> bool;
    fn set_reversing(&mut self, reverse: bool);
    fn set_pos(&mut self, pos: (f64, f64));
    fn set_hp(&mut self, hp: u16);
    fn set_alive(&mut self, alive: bool);
    // concrete
    fn render(&self, c: &graphics::context::Context, gl: &mut GlGraphics) {
        if self.is_alive() {
            image(self.get_texture(), 
                c.transform
                .trans(self.get_pos().0, self.get_pos().1)
                .rot_deg(self.get_rot())
                .scale(self.get_scale().0, self.get_scale().1),
                gl)
        }
    }
    fn should_move(&mut self, middle_screen: f64) -> u8 {
        let mut movement_type = 0;
        let mut can_fire = false;
        let buffer = self.get_middle_buffer();
        // if the gun state is in can_fire for an extended period of time, it probably should move closer to fire it
        for gun in self.get_guns() {
            if gun.can_fire() {
                can_fire = true;
            }
        }
        // return the movement type based on control status
        let current_x = self.get_pos().0;
        if self.is_player() {
            // reverse if we hit the max screen size
            if current_x >= middle_screen*2.0 {
                movement_type = 2;
                if !self.is_reversing() {
                    self.set_reversing(true);
                }
                // if we hit the other end of the screen... go back to normal
                if current_x <= 0.0 {
                    self.set_reversing(false);
                }
            }
            // else go forward
            else if !self.is_reversing() && (can_fire || current_x < middle_screen + buffer) {
                movement_type = 1;
            }
        } else {
            // reverse if we hit the max screen size
            if current_x <= 0.0 {
                movement_type = 1;
                if !self.is_reversing() {
                    self.set_reversing(true);
                }
                // if we hit the other end of the screen... go back to normal
                if current_x >= middle_screen*2.0 {
                    self.set_reversing(false);
                }
            }
            // else go forward
            else if !self.is_reversing() && (can_fire || current_x > middle_screen + buffer) {
                movement_type = 2;
            }
        }
        return movement_type
    }
    fn move_forward(&mut self, middle_screen: f64, delta_time: f64) {
        let speed = delta_time * self.get_movement_speed();
        let mut pos = self.get_pos();
        let movement_type = self.should_move(middle_screen);
        if movement_type > 0 {
            if movement_type == 1 {
                pos.0 += speed;
            } else {
                pos.0 -= speed;
            }
        }
        self.set_pos(pos);
    }
    fn take_damage(&mut self, damage: u16) -> resource::HitMarker {
        let mut hit = false;
        let mut damage_for_marker = 0;
        if self.is_alive() {
            // init an rng object
            let mut rng = rand::thread_rng();
            // roll a value between 0 and 100 percent
            let rng_roll = rng.gen_range(0..101);
            // if the roll value is a hit
            // hit values should be [0-69], miss values [70-100], for example, 30% of the shots should miss
            if rng_roll < (100.0-(100.0*self.get_evasion())) as u16 {
                hit = true;
                damage_for_marker = damage;
                // calc damage
                let hp = self.get_hp();
                if damage >= hp {
                    self.set_hp(0);
                    self.set_alive(false);
                } else  {
                    self.set_hp(hp - damage);
                }
            }
        } else {
            self.set_hp(0);
        }
        return resource::HitMarker::new(hit, damage_for_marker, self.get_pos());
    }
    fn take_damage_vec(&mut self, damage: Vec<u16>) {
        for d in damage {
            self.take_damage(d);
        }
    }
    fn reload(&mut self, delta_time: f64) {
        for gun in self.get_guns() {
            gun.reload_guns(delta_time);
        }
    }
    fn attack(&mut self, pos: (f64, f64), target_id: u16) -> Vec<weapons::Tracer> {
        let mut shots = Vec::new();
        let mut gun_pos = self.get_pos();
        let is_player = self.is_player();
        for gun in self.get_guns() {
            if gun.can_fire() {
                let damage = gun.fire();
                gun_pos.0 += gun.get_offset();
                if damage > 0 {
                    shots.push(weapons::Tracer::new(gun_pos, pos, damage, target_id, is_player, gun.get_arc_height(), gun.get_proj_speed()));
                }
            }
        }
        // print!("Ship fired! Id: {} Info: {:?}", self.get_id(), shots);
        return shots
    }
    fn can_hit(&mut self, other_pos: f64) -> bool {
        let x_pos = self.get_pos().0;
        for gun in self.get_guns() {
            if gun.can_hit(x_pos, other_pos) {
                return true
            }
        }
        return false
    }
}

pub struct Destroyer {
    id: u16,
    alive: bool,
    player: bool,
    in_reverse: bool,
    hp: u16,
    pos: (f64, f64),
    scale: (f64, f64),
    rot: f64,
    movement_speed: f64,
    middle_buffer: f64,
    evasion: f32,
    main_guns: Vec<weapons::ShipGun>,
    texture: Texture,
}
impl Destroyer {
    pub fn new(position: (f64, f64), scaled_size: (f64, f64), rotation: f64, player_controller: bool, ship_id: u16) -> Destroyer {
        let mut guns: Vec<weapons::ShipGun> = Vec::new();
        guns.push(weapons::ShipGun::new_destroyer(0.0));
        let assets: std::path::PathBuf = find_folder::Search::ParentsThenKids(3, 3).for_folder("assets").unwrap();
        Destroyer {
            id: ship_id,
            alive: true,
            hp: 100,
            pos: position,
            scale: scaled_size,
            rot: rotation,
            player: player_controller,
            movement_speed: 6.0,
            middle_buffer: 120.0,
            main_guns: guns,
            evasion: 0.4,
            in_reverse: false,
            texture: Texture::from_path(assets.join("ships/destroyer.png"),
                    &TextureSettings::new()).unwrap(),
        }
    }
}
impl Ship for Destroyer {
    // getters
    fn get_pos(&self) -> (f64, f64) { return self.pos }
    fn get_scale(&self) -> (f64, f64) { return self.scale; }
    fn get_rot(&self) -> f64 { return self.rot; }
    fn get_middle_buffer(&self) -> f64 { return self.middle_buffer; }
    fn get_movement_speed(&self) -> f64 { return self.movement_speed; }
    fn get_evasion(&self) -> f32 { return self.evasion; }
    fn get_id(&self) -> u16 { return self.id; }
    fn get_hp(&self) -> u16 { return self.hp; }
    fn get_texture(&self) -> &Texture { return &self.texture; }
    fn is_alive(&self) -> bool { self.alive }
    fn is_player(&self) -> bool { return self.player; }
    fn is_reversing(&self) -> bool { return self.in_reverse; }
    fn get_guns(&mut self) -> &mut Vec<weapons::ShipGun> { return &mut self.main_guns; }
    // setters
    fn set_pos(&mut self, pos: (f64, f64)) { self.pos = pos; }
    fn set_hp(&mut self, hp: u16) { self.hp = hp; }
    fn set_alive(&mut self, alive: bool) { self.alive = alive; }
    fn set_reversing(&mut self, reverse: bool) { self.in_reverse = reverse; } 
}

pub struct LightCruiser {
    id: u16,
    alive: bool,
    player: bool,
    in_reverse: bool,
    hp: u16,
    pos: (f64, f64),
    scale: (f64, f64),
    rot: f64,
    movement_speed: f64,
    middle_buffer: f64,
    evasion: f32,
    main_guns: Vec<weapons::ShipGun>,
    texture: Texture,
}
impl LightCruiser {
    pub fn new(position: (f64, f64), scaled_size: (f64, f64), rotation: f64, player_controller: bool, ship_id: u16) -> LightCruiser {
        let mut guns: Vec<weapons::ShipGun> = Vec::new();
        let offset = 20.0;
        guns.push(weapons::ShipGun::new_l_cruiser(-offset));
        guns.push(weapons::ShipGun::new_l_cruiser(0.0));
        guns.push(weapons::ShipGun::new_l_cruiser(offset));
        let assets: std::path::PathBuf = find_folder::Search::ParentsThenKids(3, 3).for_folder("assets").unwrap();
        LightCruiser {
            id: ship_id,
            alive: true,
            hp: 250,
            pos: position,
            scale: scaled_size,
            rot: rotation,
            player: player_controller,
            movement_speed: 5.0,
            middle_buffer: 150.0,
            main_guns: guns,
            evasion: 0.2,
            in_reverse: false,
            texture: Texture::from_path(assets.join("ships/Lcruiser.png"),
                    &TextureSettings::new()).unwrap(),
        }
    }
}
impl Ship for LightCruiser {
    // getters
    fn get_pos(&self) -> (f64, f64) { return self.pos }
    fn get_scale(&self) -> (f64, f64) { return self.scale; }
    fn get_rot(&self) -> f64 { return self.rot; }
    fn get_middle_buffer(&self) -> f64 { return self.middle_buffer; }
    fn get_movement_speed(&self) -> f64 { return self.movement_speed; }
    fn get_evasion(&self) -> f32 { return self.evasion; }
    fn get_id(&self) -> u16 { return self.id; }
    fn get_hp(&self) -> u16 { return self.hp; }
    fn get_texture(&self) -> &Texture { return &self.texture; }
    fn is_alive(&self) -> bool { self.alive }
    fn is_player(&self) -> bool { return self.player; }
    fn is_reversing(&self) -> bool { return self.in_reverse; }
    fn get_guns(&mut self) -> &mut Vec<weapons::ShipGun> { return &mut self.main_guns; }
    // setters
    fn set_pos(&mut self, pos: (f64, f64)) { self.pos = pos; }
    fn set_hp(&mut self, hp: u16) { self.hp = hp; }
    fn set_alive(&mut self, alive: bool) { self.alive = alive; }
    fn set_reversing(&mut self, reverse: bool) { self.in_reverse = reverse; } 
}

pub struct Battleship {
    id: u16,
    alive: bool,
    player: bool,
    in_reverse: bool,
    hp: u16,
    pos: (f64, f64),
    scale: (f64, f64),
    rot: f64,
    movement_speed: f64,
    middle_buffer: f64,
    main_guns: Vec<weapons::ShipGun>,
    evasion: f32,
    texture: Texture,
}
impl Battleship {
    pub fn new(position: (f64, f64), scaled_size: (f64, f64), rotation: f64, player_controller: bool, ship_id: u16) -> Battleship {
        let mut guns: Vec<weapons::ShipGun> = Vec::new();
        let gun_offset = 20.0;
        guns.push(weapons::ShipGun::new_battleship(gun_offset));
        guns.push(weapons::ShipGun::new_battleship(-gun_offset));
        let assets: std::path::PathBuf = find_folder::Search::ParentsThenKids(3, 3).for_folder("assets").unwrap();
        Battleship {
            id: ship_id,
            alive: true,
            hp: 500,
            pos: position,
            scale: scaled_size,
            rot: rotation,
            player: player_controller,
            movement_speed: 2.0,
            middle_buffer: 200.0,
            main_guns: guns,
            evasion: 0.05,
            in_reverse: false,
            texture: Texture::from_path(assets.join("ships/battleship.png"),
                    &TextureSettings::new()).unwrap(),
        }
    }
}
impl Ship for Battleship {
    // getters
    fn get_pos(&self) -> (f64, f64) { return self.pos }
    fn get_scale(&self) -> (f64, f64) { return self.scale; }
    fn get_rot(&self) -> f64 { return self.rot; }
    fn get_middle_buffer(&self) -> f64 { return self.middle_buffer; }
    fn get_movement_speed(&self) -> f64 { return self.movement_speed; }
    fn get_evasion(&self) -> f32 { return self.evasion; }
    fn get_id(&self) -> u16 { return self.id; }
    fn get_hp(&self) -> u16 { return self.hp; }
    fn get_texture(&self) -> &Texture { return &self.texture; }
    fn is_alive(&self) -> bool { self.alive }
    fn is_player(&self) -> bool { return self.player; }
    fn is_reversing(&self) -> bool { return self.in_reverse; }
    fn get_guns(&mut self) -> &mut Vec<weapons::ShipGun> { return &mut self.main_guns; }
    // setters
    fn set_pos(&mut self, pos: (f64, f64)) { self.pos = pos; }
    fn set_hp(&mut self, hp: u16) { self.hp = hp; }
    fn set_alive(&mut self, alive: bool) { self.alive = alive; }
    fn set_reversing(&mut self, reverse: bool) { self.in_reverse = reverse; } 
}

pub fn init_ships(config: &menu::ShipMenuHandler, window_width: u32, _window_height: u32) -> (FleetComposition, FleetComposition) {
    // let ship_rotation_player = 90.0;
    // let ship_rotation_ai = 270.0;
    
    // starting positions for ship generation
    // TODO update positions after each ship type
    let starting_x_from_edge = 50.0;
    let starting_y_from_edge = 80.0;
    let cruiser_buffer = 50.0;
    let destroyer_buffer = 100.0;
    
    let ship_pos_player_bs = (starting_x_from_edge, starting_y_from_edge);
    let ship_pos_ai_bs = (window_width as f64 - starting_x_from_edge, starting_y_from_edge);
    let ship_pos_player_crs = (starting_x_from_edge + cruiser_buffer, starting_y_from_edge);
    let ship_pos_player_dest = (starting_x_from_edge + destroyer_buffer, starting_y_from_edge);
    let ship_pos_ai_crs = (window_width as f64 - starting_x_from_edge - cruiser_buffer, starting_y_from_edge);
    let ship_pos_ai_dest = (window_width as f64 - starting_x_from_edge - destroyer_buffer, starting_y_from_edge);

    let ship_spacing = 30.0;
    let mut counter: u16 = 0;

    let player_destroyers = init_destroyers(config.get_destroyer_amount(true), ship_pos_player_dest, true, ship_spacing, counter);
    counter += player_destroyers.len() as u16;
    let ai_destroyers = init_destroyers(config.get_destroyer_amount(false), ship_pos_ai_dest, false, ship_spacing, counter);
    counter += ai_destroyers.len() as u16;
    let player_l_cruisers = init_l_cruisers(config.get_cruiser_amount(true), ship_pos_player_crs, true, ship_spacing, counter);
    counter += player_l_cruisers.len() as u16;
    let ai_l_cruisers = init_l_cruisers(config.get_cruiser_amount(false), ship_pos_ai_crs, false, ship_spacing, counter);
    counter += ai_l_cruisers.len() as u16;
    let player_battleships = init_battleships(config.get_battleship_amount(true), ship_pos_player_bs, true, ship_spacing, counter);
    counter += player_battleships.len() as u16;
    let ai_battleships = init_battleships(config.get_battleship_amount(false), ship_pos_ai_bs, false, ship_spacing, counter);

    let mut player_afloat_ships: Vec<Box<dyn Ship>> = Vec::new();
    let mut ai_afloat_ships: Vec<Box<dyn Ship>> = Vec::new();
    for destroyer in player_destroyers {
        player_afloat_ships.push(Box::new(destroyer));
    }
    for destroyer in ai_destroyers {
        ai_afloat_ships.push(Box::new(destroyer));
    }
    for ship in player_l_cruisers {
        player_afloat_ships.push(Box::new(ship));
    }
    for ship in ai_l_cruisers {
        ai_afloat_ships.push(Box::new(ship));
    }
    for battleship in player_battleships {
        player_afloat_ships.push(Box::new(battleship));
    }
    for battleship in ai_battleships {
        ai_afloat_ships.push(Box::new(battleship));
    }

    return (
        FleetComposition {
            afloat_ships: player_afloat_ships
        },
        FleetComposition {
            afloat_ships: ai_afloat_ships
        }
    )
}

pub fn init_destroyers(number_to_generate: u8, pos: (f64, f64), player_controlled: bool, ship_spacing: f64, mut id: u16) -> Vec<Destroyer> {
    let mut destroyers: Vec<Destroyer> = Vec::new();

    let destroyer_scale = (0.6, 0.6);
    let rotation = if player_controlled { 90.0 } else { 270.0 };
    let current_pos = pos;

    for i in 0..number_to_generate {
        destroyers.push(
            Destroyer::new((current_pos.0, current_pos.1 + (i as f64)*ship_spacing),
                            destroyer_scale, rotation, player_controlled, id)
        );
        id += 1;
    }
    destroyers
}
pub fn init_l_cruisers(number_to_generate: u8, pos: (f64, f64), player_controlled: bool, ship_spacing: f64, mut id: u16) -> Vec<LightCruiser> {
    let mut cruisers: Vec<LightCruiser> = Vec::new();

    let scale = (0.5, 0.5);
    let rotation = if player_controlled { 90.0 } else { 270.0 };
    let current_pos = pos;

    for i in 0..number_to_generate {
        cruisers.push(
            LightCruiser::new((current_pos.0, current_pos.1 + (i as f64)*ship_spacing),
                            scale, rotation, player_controlled, id)
        );
        id += 1;
    }
    cruisers
}
pub fn init_battleships(number_to_generate: u8, pos: (f64, f64), player_controlled: bool, ship_spacing: f64, mut id: u16) -> Vec<Battleship> {
    let mut battleships: Vec<Battleship> = Vec::new();

    let scale = (0.4, 0.4);
    let rotation = if player_controlled { 90.0 } else { 270.0 };
    let current_pos = pos;

    for i in 0..number_to_generate {
        battleships.push(
            Battleship::new((current_pos.0, current_pos.1 + (i as f64)*ship_spacing),
                            scale, rotation, player_controlled, id)
        );
        id += 1;
    }
    battleships
}

pub struct FleetComposition {
    pub afloat_ships: Vec<Box<dyn Ship>>
}
impl FleetComposition {
    pub fn render(&self, c: &graphics::context::Context, gl: &mut GlGraphics) {
        for ship in &self.afloat_ships {
            ship.render(c, gl);
        }
    }
    pub fn move_forward(&mut self, middle_screen: f64, delta_time: f64) {
        for ship in &mut self.afloat_ships {
            ship.move_forward(middle_screen, delta_time);
        }
    }
    pub fn update_alive_ships(&mut self) -> bool {
        self.afloat_ships.retain(|x| x.is_alive());
        if self.afloat_ships.len() > 0 {
            return true
        }
        return false
    }
    pub fn update(&mut self, delta_time: f64) {
        for ship in &mut self.afloat_ships {
            ship.reload(delta_time);
        }
    }
}
