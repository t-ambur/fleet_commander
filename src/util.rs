

pub fn get_spacing_of_string(s: &str, font_size: u32) -> f64 {
    let number_chars = s.chars().count();
    return number_chars as f64 * (font_size as f64 / 4.0);
}